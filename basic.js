/**
 *
 *
 * Change a default setting value example:
 *
 * $.person.config.{settingName} = {newValue};
 *  - or -
 * $.person({
 *      debug: true,
 *      baseUrl: 'http://api.onlinedeelname.nl/'
 * });
 *
 */
(function($) {

    var questions = false;
    var templates = false;

    $.person = function(config) {
        $.person.config = $.extend($.person.config, config);
    };
    $.person.config = {
        campaignId: false,
        debug: false,
        baseUrl: 'http://api.onlinedeelname.nl/',
        templateUrl: '/general-bin/templates.html',
        hash: ''
    };

    function sendRequest(url, type, data, successCallback, failureCallback) {
        $.ajax({
            type: type,
            url: $.person.config.baseUrl+url,
            data: data,
            success: function (response) {
                if ($.person.config.debug) {
                    console.log("[person] Ajax call success", response);
                }
                if(typeof(successCallback) === 'function') {
                    successCallback(response);
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                var response;
                try {
                    response = JSON.parse(xhr.responseText);
                    $(document).trigger('person:error', 'ajax_request_failed');
                } catch(e) {
                    $(document).trigger('person:error', 'ajax_communication');
                    response = {};
                }

                if ($.person.config.debug) {
                    console.log("[person] Ajax call failure",  xhr.responseText, response, textStatus, errorThrown);
                }

                if(typeof(failureCallback) === 'function') {
                    failureCallback(response);
                }
            }
        });
    }

    $.fn.questionByName = function(options) {

        var el = this;

        var question = {};
        $.each(questions, function(i, v) {
            if(v.name === options.name) {
                question = v;
                return false;
            }
        });
        if(question.question) {
            el.html(question.question);
        }

        return this;
    };

    $.fn.answers = function(options, elementCallback) {
        var el = this;

        var question = {};
        $.each(questions, function(i, v) {
            if(v.name === options.name) {
                question = v;
                return false;
            }
        });
        if(typeof(elementCallback) === 'function' && question.options) {
            $.each(question.options, function(i, v) {
                el.append(elementCallback(v, i));
            });
        }

        return this;
    };

    $.fn.answerByKey = function(options) {
        var el = this;

        var question = {};
        $.each(questions, function(i, v) {
            if(v.name === options.name) {
                question = v;
                return false;
            }
        });
        if(question.options) {
            var found = false;
            $.each(question.options, function(i, v) {
                if(i == options.key) {
                    el.html(v);
                    found = true;
                    return false;
                }
            });
            if(typeof(options.notFoundCallback) === 'function' && !found) {
                options.notFoundCallback();
            }
        }

        return this;
    };

    /**
     * Example implementation:
     *
     * $.person.insert({
     *      'email': "test@test.com",
     *      'firstname': "Voornaam",
     *      'lastname': "Achternaam",
     *      'lastname_prefix': "",
     *      'gender': "m",
     *      'birthdate': "2000-01-01",
     *      'address': "Adres",
     *      ...
     * }, function(result) {
     *      console.log('Gegevens met succes ingevoegd.', result);
     *      console.log(result.isNew ? 'Gegevens waren nieuw.' : 'Gegevens reeds ingevoegd.');
     * }, function(result) {
     *      console.log('Gegevens konden niet worden ingevoegd.', result);
     * });
     *
     * @param data
     * @param successCallback
     * @param failureCallback
     * @return $.person
     */
    $.person.insert = function(data, successCallback, failureCallback) {
        if(this.config.campaignId === false) {
            $(document).trigger('person:error', 'config_campaign_id');
            return this;
        }
        data.campaign_id = this.config.campaignId;
        var self = this;
        sendRequest('Person/insert', 'POST', data,
            function(response) {
                var data = (response || {}).data || {};
                if(response.success === true) {
                    if(data.new) {
                        $(document).trigger('person:newEntry');
                    } else {
                        $(document).trigger('person:existingEntry');
                    }
                    if(data.conversion) {
                        $(document).trigger('person:newConversion', [data.hash]);
                    } else {
                        $(document).trigger('person:noNewConversion');
                    }
                    if(data.hash) {
                        self.config.hash = data.hash;
                    }

                    if (typeof(successCallback) === 'function') {
                        successCallback({
                            isNew: data.new,
                            hash: data.hash
                        });
                    }
                } else {
                    if(typeof(failureCallback) === 'function') {
                        failureCallback({
                            errorMessage: self.getErrorDescription("ajax_request_failed")
                        });
                    }
                }
            },
            function(response) {
                if(typeof(failureCallback) === 'function') {
                    var error = (response || {}).error || {};
                    failureCallback({
                        errorMessage: error.error_message,
                        errorCode: error.error_code
                    });
                }
            }
        );
        return this;
    };

    $.person.answerQuestions = function(answers, successCallback, failureCallback) {
        if(this.config.hash.length === 0) {
            $(document).trigger('person:error', 'config_no_hash');
            return this;
        }
        var data = {
            campaign_id: this.config.campaignId,
            hash: this.config.hash,
            answers: answers
        };
        var self = this;

        sendRequest('Question/answer', 'POST', data,
            function(response) {
                var data = (response || {}).data || {};
                if(response.success === true) {
                    if (typeof(successCallback) === 'function') {
                        successCallback();
                    }
                } else {
                    if(typeof(failureCallback) === 'function') {
                        failureCallback({
                            errorMessage: self.getErrorDescription("ajax_request_failed")
                        });
                    }
                }
            },
            function(response) {
                if(typeof(failureCallback) === 'function') {
                    var error = (response || {}).error || {};
                    failureCallback({
                        errorMessage: error.error_message,
                        errorCode: error.error_code
                    });
                }
            }
        );
        return this;
    };

    /**
     *
     * Gets a more informative error description for a given error reason
     *
     * @param reason
     * @return string
     */
    $.person.getErrorDescription = function(reason) {
        switch(reason) {
            case "ajax_communication":
                return "Communicatiefout met de server.";
            case "ajax_request_failed":
                return "Verzoek niet succesvol afgehandeld door server.";
            case "config_campaign_id":
                return "campaignId niet geconfigureerd.";
            case "config_no_hash":
                return "Hash waarde niet aanwezig.";
        }
        return "";
    };

    /**
     *
     * Loads question data from server
     *
     * @param options
     * @param successCallback
     * @param failureCallback
     * @return $.person
     */
    $.person.getQuestions = function(options, successCallback, failureCallback) {
        if(this.config.campaignId === false) {
            $(document).trigger('person:error', 'config_campaign_id');
            return this;
        }

        var data = $.extend({
            campaign_id: this.config.campaignId,
            step: 'all'
        }, options);

        if(questions === false) {
            sendRequest('Question', 'GET', data,
                function (response) {
                    var data = (response || {}).data || {};
                    if (response.success === true) {
                        questions = data.questions ? data.questions : false;
                        if (typeof(successCallback) === 'function') {
                            successCallback({
                                questions: questions
                            });
                        }
                    } else {
                        if (typeof(failureCallback) === 'function') {
                            failureCallback({
                                errorMessage: this.getErrorDescription("ajax_request_failed")
                            });
                        }
                    }
                }
            );
        } else {
            if (typeof(successCallback) === 'function') {
                successCallback({
                    questions: questions
                });
            }
        }
        return this;
    };

    /**
     *
     * @returns {*}
     */
    $.person.getQuestionTemplates = function() {

        var deferred = when.defer();

        if (templates != false) {

            deferred.resolve(templates);

        } else {

            this.loadQuestionTemplates().done(function(response) {
                templates = response;
                deferred.resolve(templates);
            });

        }

        return deferred.promise;

    };

    /**
     *
     * @returns {*}
     */
    $.person.loadQuestionTemplates = function() {

        var deferred = when.defer();

        $.ajax({
            type: "GET",
            url: $.person.config.templateUrl,
            cache: false,
            async: true,
            success: function( response, textStatus, jqXHR) {
                deferred.resolve(response);
            },
            error: function( jqXHR, textStatus, errorThrown) {
                if ($.person.config.debug) {
                    console.error('loadQuestionTemplates error:', errorThrown);
                }
                deferred.resolve(false);
            }
        });

        return deferred.promise;

    };

    /**
     *
     * @param selector
     * @returns {*}
     */
    $.person.getQuestionTemplate = function(selector) {

        var self = this;
        var deferred = when.defer();

        // Load templates if not done yet
        self.getQuestionTemplates().done(function() {

            var el = $(selector, templates);

            // Kijken of we element gevonden hebben
            if (el.length == 1) {
                deferred.resolve(el[0].outerHTML);
            } else {
                deferred.resolve("");
            }

        });

        return deferred.promise;

    };

})(jQuery);
