# Sendt library

# Install
bower install https://stefanleenen@bitbucket.org/sendt/sendt-js.git --save

## Add javascript to HTML

```
<script type="text/javascript" src="js/lib/sendt-js/when.js"></script>
<script type="text/javascript" src="js/lib/sendt-js/base.js"></script>

<!-- Environment and API variables, we take them from PHP and put them here for javascript -->
<script type="text/javascript">
    $.sendt({
        campaignId: <?php echo json_encode(CAMPAIGN_ID); ?>,
        debug: <?php echo json_encode(DEBUG); ?>,
        baseUrl: <?php echo json_encode(BASE_API_URL); ?>
    });
</script>
```

Load when.js when using functions in Sendt.js that require promises.


# when.js library

The when.js library was added in this repository by hand. Because we 'browserified' it, so we don't have to implement AMD module loading.

Created when.js by doing the following:
(More info on https://github.com/cujojs/when/blob/master/docs/installation.md)
```
npm install -g browserify
cd /tmp
git clone https://github.com/cujojs/when
npm run browserify
cp build/when.js <sendt-js-folder>
```